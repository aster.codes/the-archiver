"""Runs the Archiver bot."""
import os

from src.client import build_bot

if os.name != "nt":
    import uvloop

    uvloop.install()

if __name__ == "__main__":
    build_bot().run()
