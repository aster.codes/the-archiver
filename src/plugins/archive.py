"""Debugger module only usable by bot owner."""
from __future__ import annotations

import asyncio
import logging
import time

import hikari
import tanjun
from tanjun.abc import SlashContext

import redis.asyncio as redis

from ..utils import converters


class RedisPoolDependency:
    def __init__(self):
        self._redis_pool = redis.ConnectionPool.from_url(
            "redis://redis", max_connections=10, decode_responses=True
        )

    def spawn_connection(self):
        return redis.Redis(connection_pool=self._redis_pool)


redis_pool = RedisPoolDependency()
logger = logging.getLogger(__name__)
component = tanjun.Component()
archive = component.with_slash_command(
    tanjun.slash_command_group("archive", "Archive a channel or section.")
)

@archive.with_command
@tanjun.with_channel_slash_option(
    "asset_channel",
    "This channel will be used to store images and assets for all `archive` commands."
)
@tanjun.with_channel_slash_option(
    "output_channel",
    "This channel will be used to store channel archive files for all `archive` commands."
)
@tanjun.as_slash_command(
    "set_channels",
    "Set channels to be used for all `/archive` commands."
)
async def set_channels(
    ctx: SlashContext,
    asset_channel: hikari.InteractionChannel,
    output_channel: hikari.InteractionChannel
):
    redis_con = redis_pool.spawn_connection()
    await redis_con.set(
        f"{ctx.guild_id}:archiver_asset_channel",
        asset_channel.id
    )
    await redis_con.set(
        f"{ctx.guild_id}:archiver_output_channel",
        output_channel.id
    )
    await ctx.respond("Set channels!!")


@archive.add_command
@tanjun.with_channel_slash_option(
        "section_to_archive",
        "The section you wish to archive"
        )
@tanjun.with_bool_slash_option(
    "dry_run",
    "Whether the channels should be deleted. (True = Not Deleted)",
    default=True
)
@tanjun.as_slash_command(
    "archive_section",
    "Archive a single channel"
)
async def archive_section(
    ctx: SlashContext,
    section_to_archive: hikari.InteractionChannel,
    dry_run: bool = True
):
    await ctx.respond(
        f"Attempting to archive {section_to_archive.name} - Dry Run: {dry_run}"
    )
    redis_con = redis_pool.spawn_connection()
    output_channel_id = await redis_con.get(
        f"{ctx.guild_id}:archiver_output_channel",
    )
    try:
        output_channel = await ctx.rest.fetch_channel(output_channel_id)
    except:
        logger.info("Output Channel finding failed")

    # Guts & Loop for archiver_asset_channel

    archive_coros = [archive_single_channel(channel, ctx, redis_con, output_channel) for channel in await ctx.rest.fetch_guild_channels(ctx.guild_id) if channel.parent_id == section_to_archive.id]

    asyncio.gather(*archive_coros)

    await ctx.respond(f"Archival of {section_to_archive.name} complete!")


async def archive_single_channel(channel, ctx, redis_con, output_channel):
    start_time = time.time()
    await ctx.respond(f"Found channel {channel.mention}, attempting to archive.")
    archives = await converters.DiscordChannelConverter.convert(
        channel,
        ctx.guild_id,
        ctx.rest,
        ctx.cache,
        redis_con
    )

    ellapsed_time = time.time() - start_time
    await ctx.respond(
        f"Archive of {channel.name} Complete! Took {ellapsed_time}",
    )

    await output_channel.send(
        f"Archive of {channel.name} Complete! Took {ellapsed_time}",
        attachments=[
            hikari.Bytes(
                page,
                f"{archives.name}-archive.html"
            )
            for page in archives._rendering
        ]
    )


@archive.add_command
@tanjun.with_channel_slash_option(
        "channel_to_archive",
        "The channel you wish to archive"
        )
@tanjun.with_bool_slash_option(
    "dry_run",
    "Whether the channel should be deleted. (True = Not Deleted)",
    default=True
)
@tanjun.as_slash_command(
    "archive_channel",
    "Archive a single channel"
)
async def archive_channel(
    ctx: SlashContext,
    channel_to_archive: hikari.InteractionChannel,
    dry_run: bool = True
):
    start_time = time.time()
    await ctx.respond(
        f"Attempting to archive {channel_to_archive.name} - Dry Run: {dry_run}"
    )
    redis_con = redis_pool.spawn_connection()
    output_channel_id = await redis_con.get(
        f"{ctx.guild_id}:archiver_output_channel",
    )
    try:
        output_channel = await ctx.rest.fetch_channel(output_channel_id)
    except:
        logger.info("Output Channel finding failed")

    archives = await converters.DiscordChannelConverter.convert(
        channel_to_archive,
        ctx.guild_id,
        ctx.rest,
        ctx.cache,
        redis_con
    )

    ellapsed_time = time.time() - start_time
    await ctx.respond(
        f"Archive of {channel_to_archive.name} Complete! Took {ellapsed_time}",
    )
    await output_channel.send(
        f"Archive of {channel_to_archive.name} Complete! Took {ellapsed_time}",
        attachments=[
            hikari.Bytes(
                page,
                f"{archives.name}-archive.html"
            )
            for page in archives._rendering
        ]
    )


loaders = component.make_loader()
