from __future__ import annotations

import hikari
import tanjun

from . import config as config_


def build_bot() -> hikari.GatewayBot:
    """Builds and returns a GatewayBot. Calls build_client to start tanjun."""
    config = config_.FullConfig.from_environ()

    bot = hikari.GatewayBot(
        config.tokens.discord_bot,
        logs=config.log_level,
        intents=config.intents,
    )

    client = build_client(bot, config=config)

    register_client_dependencies_and_callbacks(client, bot, config)

    return bot


def build_client(bot: hikari.GatewayBot, /, *, config: config_.FullConfig | None = None) -> tanjun.Client:
    """Builds and configures the tanjun client from a given GatewayBot."""
    if not config:
        config = config_.FullConfig.from_environ()

    client = tanjun.Client.from_gateway_bot(
        bot,
        mention_prefix=config.mention_prefix,
        declare_global_commands=config.declare_global_commands,
    )

    return client


def register_client_dependencies_and_callbacks(
    client: tanjun.Client,
    bot: hikari.GatewayBot,
    config: config_.FullConfig
) -> tanjun.Client:
    """Setup Client dependencies and callbacks."""

    client.load_modules("src.plugins.debug")
    client.load_modules("src.plugins.archive")
    
    # If config has owner_only set, lock bot.
    if config.owner_only:
        client.with_check(tanjun.checks.OwnerCheck())

    return client
