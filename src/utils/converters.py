from __future__ import annotations

import re
import datetime
import os
from typing import List, AsyncGenerator, Tuple, Mapping
from dataclasses import dataclass, field
import io
import logging

import redis.asyncio as redis
from hikari.users import UserImpl
from hikari.impl.entity_factory import EntityFactoryImpl
from hikari import (
    EmbedImage,
    Member,
    Role,
    User,
    Attachment,
    NotFoundError,
    ForbiddenError,
    Snowflake,
    TextableChannel,
    GatewayGuild,
    RESTGuild,
    Message,
    GuildTextChannel
)
from hikari.api.rest import RESTClient
from hikari.api.cache import Cache

import jinja2

logger = logging.getLogger(__name__)


@dataclass
class DiscordMessageConverter:

    discord_message: Message
    rendered_message_content: str
    guild: GatewayGuild | RESTGuild
    cached_attachments: List[Attachment | None]
    _rest: RESTClient
    _cache: Cache | None
    _rending: str | None
    _jinja2: jinja2.Environment
    archive_asset_func: AsyncGenerator[Message, Tuple[Attachment | EmbedImage, Message]]
    __template_name: str = "archive_message.html"

    @classmethod
    async def convert(
        cls,
        message: Message,
        guild: GatewayGuild | RESTGuild,
        rest: RESTClient,
        cache: Cache | None,
        jinja_env: jinja2.Environment,
        archive_asset_func: AsyncGenerator[
            Message, Tuple[Attachment | EmbedImage, Message]
        ]
    ):
        cached_attachments = []
        if message.attachments:
            for attachment in message.attachments:
                print("Converter Method")
                print(attachment)
                cached = await archive_asset_func.asend((attachment, message))
                if len(cached.attachments) >  0:
                    cached_attachments.append(cached.attachments[0])
        return cls(
            message,
            message.content or "",
            guild,
            cached_attachments,
            rest,
            cache,
            None,
            jinja_env,
            archive_asset_func
        )

    async def render_message_mentions(self):
        mentions: Mapping[Snowflake, User | Role] = {}
        if user_mentions := self.discord_message.user_mentions:
            mentions = user_mentions
        if role_mentions := self.discord_message.get_role_mentions():
            mentions = mentions | role_mentions

        for mention_id, mention in mentions.items():
            classes = mention_name = search_str = ""
            if isinstance(mention, (Member, UserImpl)):
                user = None
                top_role = None

                if self._cache:
                    user = self._cache.get_member(self.guild.id, mention_id)

                if user is None:
                    try:
                        user = await self._rest.fetch_member(self.guild.id, mention_id)
                        top_role = sorted(
                            (await user.fetch_roles()),
                            key=lambda r: r.position
                        )[0]

                    except:
                        user = mention

                if top_role:
                    classes = f"class='{sanitize_name(top_role.name)}'"
                mention_name = f"{user.username}#{user.discriminator}"
                search_str = f"<@!{user.id}"

            elif isinstance(user, Role):
                classes = f"class='{sanitize_name(user.name)}'"
                mention_name = user.name
                search_str = f"<@&{user.id}"

            mention_start = self.rendered_message_content.find(search_str)
            mention_end = mention_start + len(str(user.id)) + 4
            pre_mention = self.rendered_message_content[:mention_start]
            mention = f"<span {classes}>@{mention_name}</span>"
            post_mention = self.rendered_message_content[mention_end:]

            self.rendered_message_content = f"{pre_mention} {user} {post_mention}"

    async def render_custom_emoji(self):
        """Helper function to check for and replace custom discord emojis with an image that will render in html.

        Returns
        -------
        str
            A string html img element."""
        if not self.rendered_message_content:
            return ""
        custom_emojis = re.findall(r"<:\w*:\d*>", self.rendered_message_content)
        if custom_emojis:
            for custom_emoji in custom_emojis:
                start = self.rendered_message_content.find(custom_emoji)
                end = start + len(custom_emoji) + 1
                pre_emoji = self.rendered_message_content[:start]
                emoji_id = int(custom_emoji.split(":")[-1].split(">")[0])
                try:
                    demoji = self._cache.get_emoji(emoji_id)
                    if not demoji:
                        continue
                    custom_emoji = f"<img class='lozad inline emoji' title=':{demoji.name}:' data-src='{demoji.url}' />"
                    post_emoji = self.rendered_message_content[end:]
                    self.rendered_message_content = f"{pre_emoji}{custom_emoji}{post_emoji}"
                except (AttributeError, IndexError) as err:
                    logger.error(err)

    async def render_embeds(self):
        if not self.discord_message.embeds:
            return None
        entity_factory = EntityFactoryImpl(self.discord_message.app)
        embeds_data = []
        for embed in self.discord_message.embeds:
            embed_data = entity_factory.serialize_embed(embed)[0]
            if embed_data.get("image"):
                if embed.image:
                    embed_data["image"]["url"] = (
                        (await self.archive_asset_func.asend((embed.image, self.discord_message))).attachments[0].proxy_url
                    )
            if embed_data.get("thumbnail"):
                if embed.thumbnail:
                    thumbnail_asset = await self.archive_asset_func.asend((embed.thumbnail, self.discord_message))
                    embed_data["thumbnail"]["url"] = (
                        thumbnail_asset.attachments[0].proxy_url if len(thumbnail_asset.attachments) > 0 else ""
                    )
            embeds_data.append(embed_data)
        return self._jinja2.get_template("embeds.html").render(embeds=embeds_data)

    async def render(self):
        await self.render_message_mentions()
        await self.render_custom_emoji()
        member = self.discord_message.author
        top_role = None
        if not member:
            try:
                member = await self._rest.fetch_member(
                    self.guild.id, self.discord_message.author.id
                )
                top_role = sorted(
                    (await member.fetch_roles()),
                    key=lambda r: r.position,
                    reverse=True
                )[0]
            except:
                pass

        embeds = await self.render_embeds()
        tmpl = self._jinja2.get_template(self.__template_name)
        self._rendering = tmpl.render(
            message=self.discord_message,
            message_content=self.rendered_message_content,
            attachments=self.cached_attachments,
            embeds=embeds,
            class_name=sanitize_name(top_role.name if top_role else "")
        )
        return self._rendering


@dataclass
class DiscordChannelConverter:

    discord_id: Snowflake
    name: str
    messages: List[List[DiscordMessageConverter]]
    guild: GatewayGuild | RESTGuild
    _rest: RESTClient
    _cache: Cache | None
    _jinja2: jinja2.Environment
    __messages_rendering: List[io.BytesIO]
    _message_count: int = 0
    _rendering: List[str] = field(default_factory=lambda: [])
    __template_name: str = "archive_channel.html"

    @staticmethod
    def _setup_jinja2():
        tmpl_path = os.path.join(os.path.dirname(
            os.path.realpath(__name__)),
            "src/utils/jinja2_templates/archive"
        )
        file_loader = jinja2.FileSystemLoader(
                tmpl_path
        )
        return jinja2.Environment(loader=file_loader, autoescape=False)

    @classmethod
    async def convert(
        cls,
        channel: TextableChannel,
        guild_id: Snowflake,
        _rest: RESTClient,
        _cache: Cache | None,
        _redis: redis.Redis
    ):
        jinja_env = cls._setup_jinja2()
        messages: List[List[DiscordMessageConverter]] = [[], ]
        message_count = 0
        __messages_rendering = [io.BytesIO(), ]

        asset_channel_id = await _redis.get(
            f"{guild_id}:archiver_asset_channel",
        )
        
        try:
            archive_asset_channel = await _rest.fetch_channel(asset_channel_id)
        except:
            print("Cannot find asset archive channel!")

        if _cache and _cache.get_guild(guild_id):
            discord_guild = _cache.get_guild(guild_id)
        else:
            discord_guild = await _rest.fetch_guild(guild_id)

        if _cache and _cache.get_guild_channel(channel.id):
            channel = _cache.get_guild_channel(channel.id)
        else:
            channel = await _rest.fetch_channel(channel.id)

        archive_asset_func = archive_message_asset(
            archive_asset_channel,
            channel
        )
        await archive_asset_func.__anext__()

        async with channel.trigger_typing():
            async for message in channel.fetch_history().reversed():
                message_count += 1
                discord_message = await DiscordMessageConverter.convert(
                    message,
                    discord_guild,
                    _rest,
                    _cache,
                    jinja_env,
                    archive_asset_func
                )
                __messages_rendering[-1].write(
                    str.encode(
                        await discord_message.render()
                    )
                )
                messages[-1].append(
                    discord_message
                )
                if len(__messages_rendering[-1].getvalue()) >= 7500000:
                    __messages_rendering.append(io.BytesIO())

            discord_channel = cls(
                channel.id,
                channel.name,
                messages,
                discord_guild,
                _rest,
                _cache,
                jinja_env,
                __messages_rendering,
                message_count
            )

            await discord_channel.render(channel)

        return discord_channel

    async def render_guild_role_styles(self) -> str:
        roles_styles = ""
        roles = await self.guild.fetch_roles()
        for role in roles:
            roles_styles += f".{sanitize_name(role.name)} {{  color: {role.color} }}\n"

        return roles_styles

    async def render(self, channel):
        tmpl = self._jinja2.get_template(self.__template_name)
        role_styles = await self.render_guild_role_styles()
        for message in self.__messages_rendering:
            self._rendering.append(tmpl.render(
                channel=channel,
                messages=message.getvalue().decode(),
                message_count=self._message_count,
                guild=self.guild,
                timestamp=datetime.datetime.now(),
                role_styles=role_styles
            ))
        return self._rendering


async def archive_message_asset(
    asset_channel: GuildTextChannel,
    archive_channel: GuildTextChannel
) -> AsyncGenerator[Message, Tuple[Attachment | EmbedImage, Message]]:
    setup_msg = await asset_channel.send(
        f"{'=+' * 15}\n"
        "Starting New Channel Archive! \n"
        f"Channel: {archive_channel.name}\n"
        f"Channel ID: {archive_channel.id}"
    )

    print("Setup finished!")

    while True:
        print("While loop re/started")
        asset, message = yield setup_msg
        print("Recieved asset/message")
        print(message)
        print(asset)

        try:
            new_asset = await asset_channel.send(
                content=(
                    f"*Message ID:* {message.id}\n"
                    f"*Author:* {message.author.username}"
                    f"[ID: {message.author.id}]"
                ),
                attachment=asset
            )
            print("cached!!")
        except (
            AttributeError,
            NotFoundError,
            ForbiddenError
        ):
            print("Exception!")
            new_asset = await asset_channel.send(
                content=(
                    f"*Message ID:* {message.id}\n"
                    f"*Author:* {message.author.username}"
                    f"[ID: {message.author.id}]\n"
                    "```FAILED```"
                ),
                attachment="https://discord.com/assets/f9bb9c4af2b9c32a2c5ee0014661546d.png"
            )
        print("New Asset!")
        print(new_asset)
        yield new_asset

def sanitize_name(name: str):
    """Helper function to remove disallowed characters.

    Parameters
    ----------
    name: str
        A name to strip characters from.

    Returns
    -------
    str
        The string sanitized."""
    return (
        name.lower()
        .replace(" ", "-")
        .replace("'", "-")
        .replace("/", "-")
        .replace(".", "-")
        .replace("!", "-")
        .replace("=", "-")
        .replace("(", "-")
        .replace(")", "-")
    )
